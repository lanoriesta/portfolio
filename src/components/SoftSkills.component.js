import React from 'react'

const SoftSkills = () => {
          return (
                    <>
                              <section className="softSkills">
                                        <div className="softSkills__title-con">
                                                  <h2 className="softSkills__title">Soft Skills</h2>
                                        </div>
                                        <div className="softSkills__content">
                                                  <ul>
                                                            <li>
                                                                      <img src="image/check.png" alt="" /> Willingness to learn
                                                            </li>
                                                            <li>
                                                                      <img src="image/check.png" alt="" /> Focus on details
                                                            </li>
                                                            <li>
                                                                      <img src="image/check.png" alt="" /> Flexibility
                                                            </li>
                                                            <li>
                                                                      <img src="image/check.png" alt="" /> Problem - solving
                                                            </li>
                                                            <li>
                                                                      <img src="image/check.png" alt="" /> Critical Thinking
                                                            </li>
                                                            <li>
                                                                      <img src="image/check.png" alt="" /> Self - motivated
                                                            </li>
                                                            <li>
                                                                      <img src="image/check.png" alt="" /> Creativity
                                                            </li>
                                                            <li>
                                                                      <img src="image/check.png" alt="" /> Adaptability
                                                            </li>
                                                  </ul>
                                        </div>
                              </section>         
                    </>
          )
}

export default SoftSkills;
