import React from 'react'

const Skills = () => {
          return (
                    <>
                              <section className="skills">
                                        <div className="skills__title-con">
                                                  <h2 className="skills__title">Skills</h2>
                                        </div>
                                        <div className="skills__content">
                                                  <ul>
                                                            <li>
                                                            HTML{" "}
                                                                      <img className="html-img" src="image/html.png" alt="html" />
                                                                      <p>
                                                                                <span>*********</span>*
                                                                      </p>
                                                            </li>
                                                            <li>
                                                                      CSS <img className="css-img" src="image/css.png" alt="css" />
                                                                      <p>
                                                                                <span>********</span>**
                                                                      </p>
                                                            </li>
                                                            <li>
                                                            JavaScript{" "}
                                                                      <img className="js-img" src="image/javascript.png" alt="js" />
                                                                      <p>
                                                                                <span>*******</span>***
                                                                      </p>
                                                            </li>
                                                            <li>
                                                                      <img className="uiux-img" src="image/uiux.jpg" alt="uiux" />
                                                                      <p>
                                                                                <span>*******</span>***
                                                                      </p>
                                                            </li>
                                                            <li>
                                                                      <img src="image/figma.png" alt="figma" /> igma
                                                                      <p>
                                                                                <span>********</span>**
                                                                      </p>
                                                            </li>
                                                            <li>
                                                                      <img
                                                                                className="react-img"
                                                                                src="image/react.png"
                                                                                alt="react"
                                                                      />
                                                                      <p>
                                                                                <span>*******</span>***
                                                                      </p>
                                                            </li>
                                                            <li>
                                                                      <img
                                                                                className="node-img"
                                                                                src="image/nodejs-logo.png"
                                                                                alt="node"
                                                                      />
                                                                      <p>
                                                                                <span>******</span>****
                                                                      </p>
                                                            </li>
                                                  </ul>
                                        </div>
                              </section>     
                    </>
          )
}

export default Skills;
