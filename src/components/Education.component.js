import React from 'react'

const Education = () => {
          return (
                    <>
                    <section className="education">
                              <div className="education__title-con">
                                        <h2 className="education__title">Education</h2>
                              </div>
                              <div className="education__content">
                                        <h4 className="education__content-title">
                                        Bachelor of Science in Information Technology
                                        </h4>
                                        <img src="image/pup.png" alt="pup" />
                                        <p>Polytechnic University of the Phil.</p>
                                        <p>2015 - 2019</p>
                              </div>
                    </section>         
                    </>
          )
}

export default Education;
