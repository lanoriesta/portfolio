import React from 'react'

const Work = () => {
          return (
                    <>
                              <section className="work">
                                        <div className="work__title-con">
                                                  <h2 className="work__title">My Work</h2>
                                        </div>
                                        <div className="work__container">
                                                  <div className="work__content">
                                                            <a href="https://hotelsampletest1.netlify.app" target="_blank">
                                                                      <div className="work__item-con">
                                                                                <div
                                                                                style={{ backgroundImage: "url('image/website.png')" }}
                                                                                ></div>
                                                                                <h3 className="work__item-title">
                                                                                          Sample<span>Hotel</span>
                                                                                </h3>
                                                                                <p className="work__item-desc">
                                                                                {" "}
                                                                                A sample responsive website for hotels. The computer
                                                                                language I used here are HTML, CSS, Bootstrap and
                                                                                Javascript. The framework I used is React.js{" "}
                                                                                </p>
                                                                      </div>
                                                            </a>
                                                  </div>
                                                  <div className="work__content">
                                                            <a
                                                            href="https://www.figma.com/proto/qtXBcv0xRZretxUr1aD6Qw/MagisSolution?scaling=scale-down-width&page-id=0%3A1&node-id=1%3A2"
                                                            target="_blank"
                                                            >
                                                                      <div className="work__item-con">
                                                                                <div
                                                                                style={{ backgroundImage: "url('image/magis.png')" }}
                                                                                ></div>
                                                                                <h3 className="work__item-title">Magis Solution</h3>
                                                                                <p className="work__item-desc">
                                                                                {" "}
                                                                                My first project as a freelance Web Designer. A sample
                                                                                mockup design and the software I used is Figma{" "}
                                                                                </p>
                                                                      </div>
                                                            </a>
                                                  </div>
                                                  <div className="work__content">
                                                            <a
                                                            href="https://www.figma.com/proto/LMhJlFeVVuIrHWrlpgPP6N/toDoList?node-id=50%3A4&scaling=scale-down-width"
                                                            target="_blank"
                                                            >
                                                                      <div className="work__item-con">
                                                                                <div
                                                                                style={{ backgroundImage: "url('image/listToDo.png')" }}
                                                                                ></div>
                                                                                <h3 className="work__item-title">
                                                                                          List<span style={{ color: "#07EADC" }}>To</span>
                                                                                          <span style={{ color: "#E82198" }}>Do</span>
                                                                                </h3>
                                                                                <p className="work__item-desc">
                                                                                {" "}
                                                                                A sample mockup for to do list app with neon theme. The
                                                                                software I used is Figma{" "}
                                                                                </p>
                                                                      </div>
                                                            </a>
                                                  </div>
                                                  <div className="work__content">
                                                            <a
                                                            href="https://www.figma.com/proto/dkOhqsfVAGQAGWfODPwSzq/MagisProject2?node-id=2%3A2&scaling=scale-down-width&page-id=0%3A1"
                                                            target="_blank"
                                                            >
                                                                      <div className="work__item-con">
                                                                                <div
                                                                                style={{ backgroundImage: "url('image/GTP.png')" }}
                                                                                ></div>
                                                                                <h3 className="work__item-title">
                                                                                          Giving<span style={{ color: "#CE1126" }}>Tuesday</span>
                                                                                          <span style={{ color: "#0038A8" }}>Ph</span>
                                                                                </h3>
                                                                                <p className="work__item-desc">
                                                                                {" "}
                                                                                My second project as a freelance Web Designer. A sample
                                                                                mockup design for GivingTuesdayPH. The software I used is
                                                                                Figma{" "}
                                                                                </p>
                                                                      </div>
                                                            </a>
                                                  </div>
                                        </div>
                              </section>         
                    </>
          )
}

export default Work
