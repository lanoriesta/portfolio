import React from "react";

const Hero = () => {
  return (
    <>
      <section className="hero" id="Hero">
        <h4 className="hero__title-desc">Hi! My name is </h4>
        <h1 className="hero__title">Lawrence Angelo Noriesta</h1>
        <hr />
        <h2 className="hero__subtitle">
          Web Designer <span style={{ color: "#00d1f7" }}>&</span> Developer
        </h2>
        <div className="scrollDown">
          <img src="image/arrow.png" alt="arrow" />
          <img src="image/arrow.png" alt="arrow" />
          <img src="image/arrow.png" alt="arrow" />
        </div>
      </section>
    </>
  );
};

export default Hero;
