import React from "react";

const About = () => {
  return (
    <>
      <section className="about" id="about">
        <div className="about__title-con">
          <h2 className="about__title">About me</h2>
        </div>
        <div className="about__content">
          <div>
            <p>
              I'm a type of person who don't easily quit from any kind of
              challenges. An experienced website developer specializing in{" "}
              <em>HTML</em>, <em>CSS</em>, <em>React.js</em> and <em>JS</em>. I
              also love designing and creating a mockup of a single page.
            </p>
          </div>
        </div>
      </section>
    </>
  );
};

export default About;
