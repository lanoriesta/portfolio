import React from "react";

const Contact = () => {
  return (
    <>
      <section className="contact">
        <div className="contact__title-con">
          <h2 className="contact__title">Contact</h2>
        </div>
        <div className="contact__content">
          <div className="contact__number">
            <h4>
              <img src="image/phone.png" alt="phone" /> 09611551933
            </h4>
          </div>
          <div className="contact__gmail">
            <h4>
              <img src="image/gmail.png" alt="gmail" /> lanoriesta@gmail.com
            </h4>
          </div>
          <div className="contact__linkedin">
            <h4>
              <img src="image/linkedin.png" alt="linkedin" />{" "}
              linkedin.com/in/l-a-noriesta/
            </h4>
          </div>
          <div className="contact__facebook">
            <h4>
              <img src="image/fb.png" alt="facebook" /> la.noriesta
            </h4>
          </div>
          <div className="contact__twitter">
            <h4>
              <img src="image/twitter.png" alt="twitter" /> @lawrence_noriesta
            </h4>
          </div>
          <div className="contact__cv">
            <div>
              <a
                href="https://drive.google.com/uc?export=view&id=1qloZpRyyIgkj1mldScNluy_9lvvZXN3g"
                target="_blank"
              >
                <h3>My CV</h3>
              </a>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Contact;
