import { useState, useEffect, useRef } from "react";
import { animated as a, useSpring, useTransition } from "react-spring";
import { Parallax, ParallaxLayer } from "@react-spring/parallax";
import { RingLoader } from "react-spinners";
import Hero from "./components/Hero.component";
import About from "./components/About.component";
import Education from "./components/Education.component";
import SoftSkills from "./components/SoftSkills.component";
import Skills from "./components/Skills.component";
import Work from "./components/Work.component";
import Contact from "./components/Contact.component";

function App() {
  const parallax = useRef(null);

  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 1000);
  }, []);
  return (
    <div className="App">
      {loading ? (
        <RingLoader color={"#00d1f7"} loading={loading} size={150} />
      ) : (
        <Parallax pages={7} style={{ top: "0", left: "0" }} ref={parallax}>
          <ParallaxLayer
            offset={0}
            speed={1.5}
            style={{
              backgroundImage: 'url("image/HeroCover.jpg")',
              backgroundSize: "cover",
              marginBottom: ".5em",
            }}
          />
          <ParallaxLayer offset={0} speed={0.5} className="parallax_conHero">
            <Hero />
          </ParallaxLayer>
          <ParallaxLayer
            offset={1}
            speed={1}
            className="parallax-con1"
            style={{
              backgroundSize: "cover",
              backgroundImage: "url('image/AboutCover.jpg')",
            }}
          />
          <ParallaxLayer offset={1} speed={0.5}>
            <About />
          </ParallaxLayer>
          <ParallaxLayer
            offset={2}
            speed={1}
            className="parallax-con2"
            style={{
              backgroundSize: "cover",
              backgroundImage: "url('image/EductionCover.jpg')",
            }}
          />
          <ParallaxLayer offset={2} speed={0.5}>
            <Education />
          </ParallaxLayer>
          <ParallaxLayer
            offset={3}
            speed={1}
            className="parallax-con3"
            style={{ backgroundSize: "cover" }}
          />
          <ParallaxLayer offset={3} speed={0.5}>
            <SoftSkills />
          </ParallaxLayer>
          <ParallaxLayer
            offset={4}
            speed={1}
            className="parallax-con4"
            style={{
              backgroundSize: "cover",
              backgroundImage: 'url("image/skills.jpg")',
            }}
          />
          <ParallaxLayer offset={4} speed={0.5}>
            <Skills />
          </ParallaxLayer>
          <ParallaxLayer
            offset={5}
            speed={2}
            className="parallax-con5"
            style={{ backgroundSize: "cover" }}
          />
          <ParallaxLayer factor={10} offset={5} speed={0.5}>
            <Work />
          </ParallaxLayer>
          <ParallaxLayer offset={6} speed={2} style={{ background: "#202020" }}>
            <Contact />
          </ParallaxLayer>
        </Parallax>
      )}
    </div>
  );
}

export default App;
